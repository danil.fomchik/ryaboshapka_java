package com.ryaboshapka;

public class Birds {
    private String name;
    private String food;
    private String area;
    // создаем обьект класса Birds
    public Birds(String name, String food, String area) {
        this.name = name;
        this.food = food;
        this.area = area;
    }
    //Создаем геттеры для переменных food и area и name
    public String getName(){
        return name;
    }
    public String getArea() {
        return area;
    }
    public String getFood() {
        return food;
    }

    // Создаем методы для Birds
    public String swim() {
        return "Method swim: " + this.getClass().getSimpleName() + "floats over" + this.area;
    }
    public String fly() {
        return "Method fly: " + this.getClass().getSimpleName() + "flies over" + this.area;
    }
    public String eat() {
        return "Method eat: " + this.getClass().getSimpleName() + "is eating " + this.food;
    }
    public String run() {
        return "Method run: " + this.getClass().getSimpleName() + "runs over" + this.area;
    }
    public String cry() {
        return "Method cry: " + this.getClass().getSimpleName() + "is crying over" + this.area;
    }
}
