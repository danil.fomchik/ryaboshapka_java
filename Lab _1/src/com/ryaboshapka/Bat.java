package com.ryaboshapka;

// Создаем класс летучих мышей и наследуем его от класса птиц
public class Bat extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Bat(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Bat can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Bat can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Bat can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Bat can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Bat can cry over" + getArea();
    }
}
