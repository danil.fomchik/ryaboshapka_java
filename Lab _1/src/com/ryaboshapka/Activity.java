package com.ryaboshapka;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Activity {
    public int getNumber() {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }
    //Создаем массив с обьектами птиц, задаем им значения для food и area
    public List<Birds> getBirds() {
        ArrayList<Birds> birdsList = new ArrayList<>();
        birdsList.add(new Bat("Bat","insects", "cave"));
        birdsList.add(new Duck("Duck", "bread", "bread"));
        birdsList.add(new Ostrich("Ostrich","rodents", "semi-deserts"));
        birdsList.add(new Kiwi("Kiwi", "frog", "mountains"));
        birdsList.add(new Eagle("Eagle", "gophers", "mountains"));
        birdsList.add(new Swallow("Swallow","grasshoppers", "Mountains"));
        birdsList.add(new Penguin("Penguin","fish", "Antarctica"));
        return birdsList;
    }
    public void listOfBirds() {
        //Ввод числа пользователя
        System.out.println("Введите птицу");
        System.out.println("0 - Bat");
        System.out.println("1 - Duck");
        System.out.println("2 - Ostrich");
        System.out.println("3 - Kiwi");
        System.out.println("4 - Eagle");
        System.out.println("5 - Swallow");
        System.out.println("6 - Penguin");
        System.out.println("7 - Stop");
    }
    public String processFlying() {
        return "This " + getBirds().get(getNumber()).getName() + " flies over " + getBirds().get(getNumber()).getArea();
    }
    public String processEating() {
        return "This " + getBirds().get(getNumber()).getName() + " is eating " + getBirds().get(getNumber()).getFood();
    }
    public void controller() {
        while (true) {
            listOfBirds();
            switch (getNumber()) {
                case 0:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 1:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 2:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 3:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 4:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 5:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 6:
                    System.out.print(getBirds().get(getNumber()).fly());
                    System.out.print(getBirds().get(getNumber()).swim());
                    System.out.print(getBirds().get(getNumber()).run());
                    System.out.print(getBirds().get(getNumber()).cry());
                    System.out.print(processEating());
                    System.out.print(processFlying());
                    System.out.println("");
                    break;
                case 7:
                    System.out.println("Stop");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Error");
                    break;
            }
        }
    }
}
