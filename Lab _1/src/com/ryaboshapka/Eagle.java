package com.ryaboshapka;
// Создаем класс орла и наследуем его от класса птиц
public class Eagle extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Eagle(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Eagle can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Eagle can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Eagle can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Eagle can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Eagle can cry over" + getArea();
    }
}
